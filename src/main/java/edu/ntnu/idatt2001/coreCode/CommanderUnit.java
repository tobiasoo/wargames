package edu.ntnu.idatt2001.coreCode;

/**
 * The subclass CommanderUnit which is an extension of the super class Unit
 * It contains two abstract methods which determines your bonus attack damage and/or bonus resistance (plus terrain bonus),
 * but these methods are inherited from the CavalryUnit class
 * @author Tobias Orø
 */

public class CommanderUnit extends CavalryUnit {

    /**
     * Constructor for the CommanderUnit class
     * The CommanderUnit class is an extension of the class CavalryUnit, and will thus inherit everything from this class
     * @param name name of the type
     * @param health health of the unit
     */
    public CommanderUnit (String name, int health) {
            super(name, health);
        }
}