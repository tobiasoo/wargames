package edu.ntnu.idatt2001.coreCode;
import com.opencsv.CSVWriter;
import java.io.*;

/**
 * The file handler class
 * @author Tobias Orø
 */

public class CSVFileHandler {

    /**
     * Writes Army 1 units to a CSV file
     * @param army1 Human army
     */
    public void humanArmy(File army1) {
        try {
            CSVWriter write = new CSVWriter(new FileWriter("src/main/java/File writing/HumanArmy.csv"));
            write.writeNext(new String[]{army1.toString()});
            write.flush();
            write.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Writes Army 2 units to a CSV file
     * @param army2 Devil army
     */
    public void devilArmy(File army2) {

        try {
            CSVWriter write = new CSVWriter(new FileWriter("src/main/java/File writing/DevilArmy.csv"));
            write.writeNext(new String[]{army2.toString()});
            write.flush();
            write.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Reads the Army 1 CSV file
     * @return the units that has been added
     */

    public String readHumanArmy() {

        String path = "src/main/java/File writing/HumanArmy.csv";
        String line = "";
        StringBuilder line2 = new StringBuilder();

        try {
            BufferedReader br = new BufferedReader(new FileReader(path));

            while ((line  = br.readLine()) != null) {
                line2.append(line.replace("\"", "")).append("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } return line2.toString();
    }

    /**
     * Reads the Army 2 CSV file
     * @return the units that has been added
     */

    public String readDevilArmy () {

        String path = "src/main/java/File writing/DevilArmy.csv";
        String line;
        StringBuilder line2 = new StringBuilder();

        try {
            BufferedReader br = new BufferedReader(new FileReader(path));

            while ((line  = br.readLine()) != null) {
                line2.append(line.replace("\"", "")).append("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } return line2.toString();
    }
}


