package edu.ntnu.idatt2001.coreCode;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.io.File;

/**
 * The start application class which is a javafx class
 * @author Tobias Orø
 */

public class WarGamesApplication extends Application {

    /**
     * The front page scene
     * @param primaryStage Sets the scene and opens the front page of the application
     * @throws Exception throws an exception if something goes wrong
     */
    @Override
    public void start(Stage primaryStage) throws Exception {

        FXMLLoader loader = new FXMLLoader(new File("src/main/resources/edu.ntnu.idatt2001.coreCode/FrontPage.fxml").toURI().toURL());
        Parent root = loader.load();
        primaryStage.setTitle("War Games");
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * Launches the application
     * @param args launching the application
     */
    public static void main(String[] args) {
        launch(args);
    }
}
