package edu.ntnu.idatt2001.coreCode;

/**
 * The abstract class Unit
 * It contains all the attributes that all the child classes need to create a unit
 * @author Tobias Orø
 */

public abstract class Unit {
    /**
     * Attributes for the superclass Unit
     */

    private String name;
    private int health;
    private int attack;
    private int armor;

    /**
     * Constructor
     * @param name name of the unit
     * @param health health points the unit has
     * @param attack attack damage for the unit
     * @param armor armor for the unit
     */
    public Unit (String name, int health, int attack, int armor) {
        this.name = name;
        this.health = health;
        this.attack = attack;
        this.armor = armor;
    }

    /**
     * Formula for attack
     * After the attack it shows the updated health for the opponent
     * @param opponent the attack damage one unit does to another
     */
    public void attack (Unit opponent, String terrain) {
        opponent.health = opponent.health - (this.attack + this.getAttackBonus() + this.getTerrainAttackBonus(terrain)) +
                (opponent.armor + opponent.getResistBonus() + opponent.getTerrainResistBonus(terrain));
    }

    /**
     * Getter for name
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Getter for health
     * @return health
     */
    public int getHealth() {
        return health;
    }

    /**
     * Getter for attack
     * @return attack
     */
    public int getAttack() {
        return attack;
    }

    /**
     * Setter for health
     * @param health health points
     */
    public void setHealth(int health) {
        if (health >= 0) {
            this.health = health;
        }
    }

    /**
     * Getter for armor
     * @return armor
     */
    public int getArmor() {
        return armor;
    }

    /**
     * Bonus attack damage
     */
    public abstract int getAttackBonus ();

    /**
     * Bonus resistance
     */
    public abstract int getResistBonus ();

    /**
     * Terrain bonus attack damage
     * @param terrain the terrain that the battle will take place in
     * @return the terrain attack bonus damage
     */
    public abstract int getTerrainAttackBonus(String terrain);
    /**
     * Terrain bonus resistance
     * @param terrain the terrain that the battle will take place in
     * @return the terrain bonus resistance
     */
    public abstract int getTerrainResistBonus(String terrain);

    /**
     * toString method that converts the attributes to the String class
     * Tried to fit this to the CSV file...
     * @return name + health
     */
    @Override
    public String toString() {
        return "\n" + name + ", " + health;
    }

}