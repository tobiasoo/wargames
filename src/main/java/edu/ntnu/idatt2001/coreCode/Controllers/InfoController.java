package edu.ntnu.idatt2001.coreCode.Controllers;

import edu.ntnu.idatt2001.coreCode.Unit;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import java.io.File;
import java.io.IOException;

/**
 * The info controller class
 * It stores info about the two armies
 * @author Tobias Orø
 */

public class InfoController {

    /**
     * Labels that are displayed on the screen
     */
    @FXML Label nameInf1, healthInf1, attackDmgInf1, armorInf1, bonusDamageInf1, bonusArmorInf1, unitsInf1;
    @FXML Label nameRang1, healthRang1, attackDmgRanged1, armorRanged1, bonusDamageRanged1, bonusArmorRanged1, unitsRanged1;
    @FXML Label nameCav1, healthCav1, attackDmgCav1, armorCav1, bonusDamageCav1, bonusArmorCav1, unitsCav1;
    @FXML Label nameCom1, healthCom1, attackDmgCom1, armorCom1, bonusDamageCom1, bonusArmorCom1, unitsCom1;

    @FXML Label nameInf2, healthInf2, attackDmgInf2, armorInf2, bonusDamageInf2, bonusArmorInf2, unitsInf2;
    @FXML Label nameRang2, healthRang2, attackDmgRanged2, armorRanged2, bonusDamageRanged2, bonusArmorRanged2, unitsRanged2;
    @FXML Label nameCav2, healthCav2, attackDmgCav2, armorCav2, bonusDamageCav2, bonusArmorCav2, unitsCav2;
    @FXML Label nameCom2, healthCom2, attackDmgCom2, armorCom2, bonusDamageCom2, bonusArmorCom2, unitsCom2;

    /**
     * Switches back to the second page of the application
     * @param event back button being pressed
     * @throws IOException throws an exception if something goes wrong
     */
    public void switchBackToScene2(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(new File("src/main/resources/edu.ntnu.idatt2001.coreCode/SecondPage.fxml").toURI().toURL());
        Parent root = loader.load();
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Scene scene = new Scene(root);
        window.setScene(scene);
        window.show();
    }

    /**
     * Displays the data from the unit on the screen
     * @param unit Infantry Unit
     * @param units How many units that the user has added in the previous page
     */
    public void displayDataInfantry1 (Unit unit, int units) {
        nameInf1.setText(unit.getName());
        healthInf1.setText(Integer.toString(unit.getHealth()));
        attackDmgInf1.setText(Integer.toString(unit.getAttack()));
        armorInf1.setText(Integer.toString(unit.getArmor()));
        bonusDamageInf1.setText(Integer.toString(unit.getAttackBonus()));
        bonusArmorInf1.setText(Integer.toString(unit.getResistBonus()));
        unitsInf1.setText(Integer.toString(units));
    }
    /**
     * Displays the data from the unit on the screen
     * @param unit Ranged Unit
     * @param units How many units that the user has added in the previous page
     */
    public void displayDataRanged1 (Unit unit, int units) {
        nameRang1.setText(unit.getName());
        healthRang1.setText(Integer.toString(unit.getHealth()));
        attackDmgRanged1.setText(Integer.toString(unit.getAttack()));
        armorRanged1.setText(Integer.toString(unit.getArmor()));
        bonusDamageRanged1.setText(Integer.toString(unit.getAttackBonus()));
        bonusArmorRanged1.setText(Integer.toString(unit.getResistBonus()));
        unitsRanged1.setText(Integer.toString(units));
    }
    /**
     * Displays the data from the unit on the screen
     * @param unit Cavalry Unit
     * @param units How many units that the user has added in the previous page
     */
    public void displayDataCavalry1 (Unit unit, int units) {
        nameCav1.setText(unit.getName());
        healthCav1.setText(Integer.toString(unit.getHealth()));
        attackDmgCav1.setText(Integer.toString(unit.getAttack()));
        armorCav1.setText(Integer.toString(unit.getArmor()));
        bonusDamageCav1.setText(Integer.toString(unit.getAttackBonus()));
        bonusArmorCav1.setText(Integer.toString(unit.getResistBonus()));
        unitsCav1.setText(Integer.toString(units));
    }
    /**
     * Displays the data from the unit on the screen
     * @param unit Commander Unit
     * @param units How many units that the user has added in the previous page
     */
    public void displayDataCommander1 (Unit unit, int units) {
        nameCom1.setText(unit.getName());
        healthCom1.setText(Integer.toString(unit.getHealth()));
        attackDmgCom1.setText(Integer.toString(unit.getAttack()));
        armorCom1.setText(Integer.toString(unit.getArmor()));
        bonusDamageCom1.setText(Integer.toString(unit.getAttackBonus()));
        bonusArmorCom1.setText(Integer.toString(unit.getResistBonus()));
        unitsCom1.setText(Integer.toString(units));
    }

    /**
     * Displays the data from the unit on the screen
     * @param unit Infantry Unit
     * @param units How many units that the user has added in the previous page
     */
    public void displayDataInfantry2 (Unit unit, int units) {
        nameInf2.setText(unit.getName());
        healthInf2.setText(Integer.toString(unit.getHealth()));
        attackDmgInf2.setText(Integer.toString(unit.getAttack()));
        armorInf2.setText(Integer.toString(unit.getArmor()));
        bonusDamageInf2.setText(Integer.toString(unit.getAttackBonus()));
        bonusArmorInf2.setText(Integer.toString(unit.getResistBonus()));
        unitsInf2.setText(Integer.toString(units));
    }
    /**
     * Displays the data from the unit on the screen
     * @param unit Ranged Unit
     * @param units How many units that the user has added in the previous page
     */
    public void displayDataRanged2 (Unit unit, int units) {
        nameRang2.setText(unit.getName());
        healthRang2.setText(Integer.toString(unit.getHealth()));
        attackDmgRanged2.setText(Integer.toString(unit.getAttack()));
        armorRanged2.setText(Integer.toString(unit.getArmor()));
        bonusDamageRanged2.setText(Integer.toString(unit.getAttackBonus()));
        bonusArmorRanged2.setText(Integer.toString(unit.getResistBonus()));
        unitsRanged2.setText(Integer.toString(units));
    }
    /**
     * Displays the data from the unit on the screen
     * @param unit Cavalry Unit
     * @param units How many units that the user has added in the previous page
     */
    public void displayDataCavalry2 (Unit unit, int units) {
        nameCav2.setText(unit.getName());
        healthCav2.setText(Integer.toString(unit.getHealth()));
        attackDmgCav2.setText(Integer.toString(unit.getAttack()));
        armorCav2.setText(Integer.toString(unit.getArmor()));
        bonusDamageCav2.setText(Integer.toString(unit.getAttackBonus()));
        bonusArmorCav2.setText(Integer.toString(unit.getResistBonus()));
        unitsCav2.setText(Integer.toString(units));
    }
    /**
     * Displays the data from the unit on the screen
     * @param unit Commander Unit
     * @param units How many units that the user has added in the previous page
     */
    public void displayDataCommander2 (Unit unit, int units) {
        nameCom2.setText(unit.getName());
        healthCom2.setText(Integer.toString(unit.getHealth()));
        attackDmgCom2.setText(Integer.toString(unit.getAttack()));
        armorCom2.setText(Integer.toString(unit.getArmor()));
        bonusDamageCom2.setText(Integer.toString(unit.getAttackBonus()));
        bonusArmorCom2.setText(Integer.toString(unit.getResistBonus()));
        unitsCom2.setText(Integer.toString(units));
    }
}
