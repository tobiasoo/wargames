package edu.ntnu.idatt2001.coreCode.Controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.io.File;
import java.io.IOException;

/**
 * The front page controller for the application
 * @author Tobias Orø
 */

public class FrontPageController {

    /**
     * Switches scenes from the front page to the second page
     * @param event Button being pressed
     * @throws IOException Throws an exception if something goes wrong
     */
    public void switchToScene2(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(new File("src/main/resources/edu.ntnu.idatt2001.coreCode/SecondPage.fxml").toURI().toURL());
        Parent root = loader.load();
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        Scene scene = new Scene(root);
        window.setScene(scene);
        window.show();
    }
}

