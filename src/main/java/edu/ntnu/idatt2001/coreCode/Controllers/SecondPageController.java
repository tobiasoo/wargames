package edu.ntnu.idatt2001.coreCode.Controllers;

import edu.ntnu.idatt2001.coreCode.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.paint.Paint;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * The second page of the application, or in other words, the main page
 * In this class you can add units to armies and make them fight each other
 * You can also choose terrain before fighting
 * @author Tobias Orø
 */

public class SecondPageController {


    /**
     * Creating two armies and a battle
     */
    private final Army army1 = new Army("Human army");
    private final Army army2 = new Army("Devil army");
    private final Battle battle = new Battle(army1, army2);

    /**
     * Creating a CSV writer and reader
     */
    private final CSVFileHandler csv = new CSVFileHandler();

    private String terrain = "";


    /**
     * The number of units for each unit
     */
    private int infUnitList1, rangUnitList1, cavUnitList1, comUnitList1, infUnitList2, rangUnitList2, cavUnitList2, comUnitList2;

    /**
     * Texts and Textfields which are displayed on the screen
     */
    @FXML
    private TextField army1Infantry, army1Ranged, army1Cavalry, army1Commander;
    @FXML
    private TextField army2Infantry, army2Ranged, army2Cavalry, army2Commander, chooseTerrain;
    @FXML
    private Text getFeedback,
            army1InfoInf, army1InfoRanged, army1InfoCavalry, army1InfoCommander,
            army2InfoInf, army2InfoRanged, army2InfoCavalry, army2InfoCommander,
            clearList1, clearList2;
    @FXML
    private Text battleInfo;
    @FXML
    private Text terrainConfirmation;

    /**
     * The add infantry button for army 1
     * Adds the amount of units into army 1
     */
    public void addInfantryButtonForArmy1() {

        try {
            if (army1Infantry.getText().equals("0")) {
                getFeedback.setFill(Paint.valueOf("#bf0a0a"));
                getFeedback.setText("You can't add 0 units!");
            } else if (army1Infantry.getText().isEmpty()) {
                getFeedback.setFill(Paint.valueOf("#bf0a0a"));
                getFeedback.setText("You have to write something!");
            } else {
                infUnitList1 = Integer.parseInt(army1Infantry.getText().trim());
                ArrayList<Unit> inf = UnitFactory.getUnitsSortedByType(infUnitList1, "Pikemen", 100);
                army1.addAll(inf);
                army1InfoInf.setFill(Paint.valueOf("#1c8f1a"));
                army1InfoInf.setText(infUnitList1 + " Pikemen added!");
                getFeedback.setText("");
            }
        } catch (NumberFormatException n) {
            getFeedback.setFill(Paint.valueOf("#bf0a0a"));
            getFeedback.setText("You have to write a number!");
        } catch (Exception e) {
            getFeedback.setFill(Paint.valueOf("#bf0a0a"));
            getFeedback.setText("Error");
        }
    }

    /**
     * The add ranged button for army 1
     * Adds the amount of units into army 1
     */
    public void addRangedButtonForArmy1() {

        try {
            if (army1Ranged.getText().equals("0")) {
                getFeedback.setFill(Paint.valueOf("#bf0a0a"));
                getFeedback.setText("You can't add 0 units!");
            }
            else if (army1Ranged.getText().isBlank()) {
                getFeedback.setFill(Paint.valueOf("#bf0a0a"));
                getFeedback.setText("You have to write something!");
            } else {
                rangUnitList1 = Integer.parseInt(army1Ranged.getText().trim());
                ArrayList<Unit> rang = UnitFactory.getUnitsSortedByType(rangUnitList1, "Marksman", 100);
                army1.addAll(rang);
                army1InfoRanged.setFill(Paint.valueOf("#1c8f1a"));
                army1InfoRanged.setText(rangUnitList1 + " Marksman added!");
                getFeedback.setText("");
            }
        } catch (NumberFormatException n) {
            getFeedback.setFill(Paint.valueOf("#bf0a0a"));
            getFeedback.setText("You have to write a number!");
        } catch (Exception e) {
            getFeedback.setText("Error");
        }
    }

    /**
     * The add cavalry button for army 1
     * Adds the amount of units into army 1
     */
    public void addCavalryButtonForArmy1() {

        try {
            if (army1Cavalry.getText().equals("0")) {
                getFeedback.setFill(Paint.valueOf("#bf0a0a"));
                getFeedback.setText("You can't add 0 units!");
            } else if (army1Cavalry.getText().isEmpty()) {
                getFeedback.setFill(Paint.valueOf("#bf0a0a"));
                getFeedback.setText("You have to write something!");
            } else {
                cavUnitList1 = Integer.parseInt(army1Cavalry.getText().trim());
                ArrayList<Unit> cav = UnitFactory.getUnitsSortedByType(cavUnitList1, "Knight", 100);
                army1.addAll(cav);
                army1InfoCavalry.setFill(Paint.valueOf("#1c8f1a"));
                army1InfoCavalry.setText(cavUnitList1 + " Knights added!");
                getFeedback.setText("");
            }
        } catch (NumberFormatException n) {
            getFeedback.setFill(Paint.valueOf("#bf0a0a"));
            getFeedback.setText("You have to write a number!");
        } catch (Exception e) {
            getFeedback.setFill(Paint.valueOf("#bf0a0a"));
            getFeedback.setText("Error");
        }
    }

    /**
     * The add commander button for army 1
     * Adds the amount of units into army 1
     */
    public void addCommanderButtonForArmy1() {

        try {
            if (army1Commander.getText().equals("0")) {
                getFeedback.setFill(Paint.valueOf("#bf0a0a"));
                getFeedback.setText("You can't add 0 units!");
            }
            else if (army1Commander.getText().isEmpty()) {
                getFeedback.setFill(Paint.valueOf("#bf0a0a"));
                getFeedback.setText("You have to write something!");
            } else {
                comUnitList1 = Integer.parseInt(army1Commander.getText().trim());
                ArrayList<Unit> com = UnitFactory.getUnitsSortedByType(comUnitList1, "Arch Angel", 180);
                army1.addAll(com);
                army1InfoCommander.setFill(Paint.valueOf("#1c8f1a"));
                army1InfoCommander.setText(comUnitList1 + " Arch Angels added!");
                getFeedback.setText("");
            }
        } catch (NumberFormatException n) {
            getFeedback.setFill(Paint.valueOf("#bf0a0a"));
            getFeedback.setText("You have to write a number!");
        } catch (Exception e) {
            getFeedback.setFill(Paint.valueOf("#bf0a0a"));
            getFeedback.setText("Error");
        }
    }

    /**
     * Clears the list of units for army 1
     */
    public void clearListArmy1() {
        if (army1.getAllUnits().isEmpty()) {
            getFeedback.setFill(Paint.valueOf("#bf0a0a"));
            getFeedback.setText("The list is already empty!");
        } else {
            army1.getAllUnits().clear();
            army1InfoInf.setText("");
            army1InfoRanged.setText("");
            army1InfoCavalry.setText("");
            army1InfoCommander.setText("");
            clearList1.setFill(Paint.valueOf("#1c8f1a"));
            clearList1.setText("List is now empty!");
            army1Infantry.setText("");
        }
    }

    /**
     * The add infantry button for army 2
     * Adds the amount of units into army 2
     */
    public void addInfantryButtonForArmy2() {

        try {
            if (army2Infantry.getText().equals("0")) {
                getFeedback.setFill(Paint.valueOf("#bf0a0a"));
                getFeedback.setText("You can't add 0 units!");
            }
            else if (army2Infantry.getText().isEmpty()) {
                getFeedback.setFill(Paint.valueOf("#bf0a0a"));
                getFeedback.setText("You have to write something!");
                throw new IllegalArgumentException();
            } else {
                infUnitList2 = Integer.parseInt(army2Infantry.getText().trim());
                ArrayList<Unit> inf = UnitFactory.getUnitsSortedByType(infUnitList2, "Imp", 100);
                army2.addAll(inf);
                army2InfoInf.setFill(Paint.valueOf("#1c8f1a"));
                army2InfoInf.setText(infUnitList2 + " Imps added!");
                getFeedback.setText("");
            }
        } catch (NumberFormatException n) {
            getFeedback.setFill(Paint.valueOf("#bf0a0a"));
            getFeedback.setText("You have to write a number!");
        } catch (Exception e) {
            getFeedback.setFill(Paint.valueOf("#bf0a0a"));
            getFeedback.setText("Error");
        }
    }

    /**
     * The add ranged button for army 2
     * Adds the amount of units into army 2
     */
    public void addRangedButtonForArmy2() {

        try {
            if (army2Ranged.getText().equals("0")) {
                getFeedback.setFill(Paint.valueOf("#bf0a0a"));
                getFeedback.setText("You can't add 0 units!");
            }
            else if (army2Ranged.getText().isBlank()) {
                getFeedback.setFill(Paint.valueOf("#bf0a0a"));
                getFeedback.setText("You have to write something!");
            } else {
                rangUnitList2 = Integer.parseInt(army2Ranged.getText().trim());
                ArrayList<Unit> rang = UnitFactory.getUnitsSortedByType(rangUnitList2, "Magogs", 100);
                army2.addAll(rang);
                army2InfoRanged.setFill(Paint.valueOf("#1c8f1a"));
                army2InfoRanged.setText(rangUnitList2 + " Magogs added!");
                getFeedback.setText("");
            }
        } catch (NumberFormatException n) {
            getFeedback.setFill(Paint.valueOf("#bf0a0a"));
            getFeedback.setText("You have to write a number!");
        } catch (Exception e) {
            getFeedback.setFill(Paint.valueOf("#bf0a0a"));
            getFeedback.setText("Error");
        }
    }

    /**
     * The add cavalry button for army 2
     * Adds the amount of units into army 2
     */
    public void addCavalryButtonForArmy2() {

        try {
            if (army2Cavalry.getText().equals("0")) {
                getFeedback.setFill(Paint.valueOf("#bf0a0a"));
                getFeedback.setText("You can't add 0 units!");
            }
            else if (army2Cavalry.getText().isBlank()) {
                getFeedback.setFill(Paint.valueOf("#bf0a0a"));
                getFeedback.setText("You have to write something!");
            } else {
                cavUnitList2 = Integer.parseInt(army2Cavalry.getText().trim());
                ArrayList<Unit> cav = UnitFactory.getUnitsSortedByType(cavUnitList2, "Dread Knight", 100);
                army2.addAll(cav);
                army2InfoCavalry.setFill(Paint.valueOf("#1c8f1a"));
                army2InfoCavalry.setText(cavUnitList2 + " Dread Knights added!");
                getFeedback.setText("");
            }

        } catch (NumberFormatException n) {
            getFeedback.setFill(Paint.valueOf("#bf0a0a"));
            getFeedback.setText("You have to write a number!");
        } catch (Exception e) {
            getFeedback.setFill(Paint.valueOf("#bf0a0a"));
            getFeedback.setText("Error");
        }
    }

    /**
     * The add commander button for army 2
     * Adds the amount of units into army 2
     */
    public void addCommanderButtonForArmy2() {

        try {
            if (army2Commander.getText().equals("0")) {
                getFeedback.setFill(Paint.valueOf("#bf0a0a"));
                getFeedback.setText("You can't add 0 units!");
            }
            else if (army2Commander.getText().isEmpty()) {
                getFeedback.setFill(Paint.valueOf("#bf0a0a"));
                getFeedback.setText("You have to write something!");
            } else {
                comUnitList2 = Integer.parseInt(army2Commander.getText().trim());
                ArrayList<Unit> com = UnitFactory.getUnitsSortedByType(comUnitList2, "Arch Devil", 180);
                army2.addAll(com);
                army2InfoCommander.setFill(Paint.valueOf("#1c8f1a"));
                army2InfoCommander.setText(comUnitList2 + " Arch Devils added!");
                getFeedback.setText("");
            }
        } catch (NumberFormatException n) {
            getFeedback.setFill(Paint.valueOf("#bf0a0a"));
            getFeedback.setText("You have to write a number!");
        } catch (Exception e) {
            getFeedback.setFill(Paint.valueOf("#bf0a0a"));
            getFeedback.setText("Error");
        }
    }

    /**
     * Clears the list of units for army 2
     */
    public void clearListArmy2() {
        if (army2.getAllUnits().isEmpty()) {
            getFeedback.setFill(Paint.valueOf("#bf0a0a"));
            getFeedback.setText("The list is already empty!");
        } else {
            army2.getAllUnits().clear();
            army2InfoInf.setText("");
            army2InfoRanged.setText("");
            army2InfoCavalry.setText("");
            army2InfoCommander.setText("");
            clearList2.setFill(Paint.valueOf("#1c8f1a"));
            clearList2.setText("List is now empty!");
        }
        clearList2.setText("");
    }

    /**
     * Switches to the InfoPage which displays the unit information for the user
     * @param event View army details button being pressed
     * @throws IOException throws an exception if something goes wrong
     */
    public void switchToScene3(ActionEvent event) throws IOException {

        FXMLLoader loader = new FXMLLoader(new File("src/main/resources/edu.ntnu.idatt2001.coreCode/InfoPage.fxml").toURI().toURL());
        Parent root = loader.load();
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Scene scene = new Scene(root);
        window.setScene(scene);
        window.show();

        InfoController infoController = loader.getController();

        // Army 1
        Unit unit1 = new InfantryUnit("Pikemen", 100);
        Unit unit2 = new RangedUnit("Marksman", 100);
        Unit unit3 = new CavalryUnit("Knight", 100);
        Unit unit4 = new CommanderUnit("Arch Angel", 180);
        infoController.displayDataInfantry1(unit1, infUnitList1);
        infoController.displayDataRanged1(unit2, rangUnitList1);
        infoController.displayDataCavalry1(unit3, cavUnitList1);
        infoController.displayDataCommander1(unit4, comUnitList1);
        // Army 2
        Unit unit5 = new InfantryUnit("Imp", 100);
        Unit unit6 = new RangedUnit("Magogs", 100);
        Unit unit7 = new CavalryUnit("Dread Knight", 100);
        Unit unit8 = new CommanderUnit("Arch Devil", 180);
        infoController.displayDataInfantry2(unit5, infUnitList2);
        infoController.displayDataRanged2(unit6, rangUnitList2);
        infoController.displayDataCavalry2(unit7, cavUnitList2);
        infoController.displayDataCommander2(unit8, comUnitList2);

    }

    /**
     * Writes army 1 to a CSV file
     */
    public void writeArmy1 () {
        csv.humanArmy(new File(army1.toString()));
    }

    /**
     * Writes army 2 to a CSV file
     */
    public void writeArmy2 () {
        csv.devilArmy(new File(army2.toString()));
    }

    /**
     * Reads army 1 to a CSV file
     * @param event Switches scenes when the Read button is being pressed
     * @throws IOException throws and exception if something goes wrong
     */
    public void readArmy1 (ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(new File("src/main/resources/edu.ntnu.idatt2001.coreCode/CSVPage.fxml").toURI().toURL());
        Parent root = loader.load();
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Scene scene = new Scene(root);
        window.setScene(scene);
        window.show();
        csv.readHumanArmy();
    }

    /**
     * Reads army 2 to a CSV file
     * @param event Switches scenes when the Read button is being pressed
     * @throws IOException throws and exception if something goes wrong
     */
    public void readArmy2 (ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(new File("src/main/resources/edu.ntnu.idatt2001.coreCode/CSVPage.fxml").toURI().toURL());
        Parent root = loader.load();
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Scene scene = new Scene(root);
        window.setScene(scene);
        window.show();
        csv.readDevilArmy();
    }

    /**
     * Method for choosing terrain
     * The user can choose between "Forest", "Hill" or "Plains" which give different bonuses for the units depending on what terrain
     * they excel in
     */
    public void chooseTerrains() {
        if (chooseTerrain.getText().isEmpty()) {
            terrainConfirmation.setFill(Paint.valueOf("#bf0a0a"));
            terrainConfirmation.setText("You have to write something!");
        }
        else if (chooseTerrain.getText().equalsIgnoreCase("Forest") || chooseTerrain.getText().equalsIgnoreCase("Hill") || chooseTerrain.getText().equalsIgnoreCase("Plains")) {
                terrain = chooseTerrain.getText();
                terrainConfirmation.setFill(Paint.valueOf("#1c8f1a"));
                terrainConfirmation.setText(terrain + " terrain chosen!");

        } else {
            terrainConfirmation.setFill(Paint.valueOf("#bf0a0a"));
            terrainConfirmation.setText("You have to write either Forest, Hill or Plains!");
        }
    }

    /**
     * The battle simulation method
     * The user adds units to two armies and make them fight each other
     * The winner will be declared in the end with its remaining units
     */
    public void simulate () {

        if (!army1.hasUnits() && !army2.hasUnits()) {
            battleInfo.setFill(Paint.valueOf("#bf0a0a"));
            battleInfo.setText("You have to add units to fight!");
        }
        else if (!army1.hasUnits() && army2.hasUnits()) {
            battleInfo.setFill(Paint.valueOf("#bf0a0a"));
            battleInfo.setText("You have to add units for Army 1.");
        }
        else if (!army2.hasUnits() && army1.hasUnits()) {
            battleInfo.setFill(Paint.valueOf("#bf0a0a"));
            battleInfo.setText("You have to add units for Army 2.");
        }
        else if (army1.hasUnits() && army2.hasUnits()) {
            Army winner = battle.simulate(terrain);
            if (winner.equals(army1)) {
                battleInfo.setFill(Paint.valueOf("#8c3dc2"));
                battleInfo.setText("The winner is Army 1!" + "\nRemaining units: " + army1.showUnits() + " " + army1.getName());

                army1InfoInf.setText("");
                army1InfoRanged.setText("");
                army1InfoCavalry.setText("");
                army1InfoCommander.setText("");
                army2InfoInf.setText("");
                army2InfoRanged.setText("");
                army2InfoCavalry.setText("");
                army2InfoCommander.setText("");
                clearListArmy1();
                clearListArmy2();
                clearList1.setText("");
                clearList2.setText("");
                terrainConfirmation.setText("");
            } else {
                battleInfo.setFill(Paint.valueOf("#8c3dc2"));
                battleInfo.setText("The winner is Army 2!" + "\nRemaining units: " + army2.showUnits() + " " + army2.getName());

                army1InfoInf.setText("");
                army1InfoRanged.setText("");
                army1InfoCavalry.setText("");
                army1InfoCommander.setText("");
                army2InfoInf.setText("");
                army2InfoRanged.setText("");
                army2InfoCavalry.setText("");
                army2Commander.setText("");
                clearList1.setText("");
                clearList2.setText("");
                clearListArmy1();
                clearListArmy2();
                terrainConfirmation.setText("");
            }
            getFeedback.setText("");
        }
    }
}

