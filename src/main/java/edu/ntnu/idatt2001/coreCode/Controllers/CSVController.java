package edu.ntnu.idatt2001.coreCode.Controllers;

import edu.ntnu.idatt2001.coreCode.CSVFileHandler;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;
import java.io.File;
import java.io.IOException;

/**
 * Controller page for the CSVFileHandler class
 * The first method switches back to the second page of the Application
 * The second method initializes the read army buttons from the second page of the App
 * @author Tobias Orø
 */

public class CSVController {

    /**
     * TextAreas on the screen
     */
    @FXML
    private TextArea area1, area2;

    /**
     * Switches scenes to the second page
     * @param event Back button being pressed
     * @throws IOException throws an exception if something goes wrong
     */
    public void switchBackToScene2(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(new File("src/main/resources/edu.ntnu.idatt2001.coreCode/SecondPage.fxml").toURI().toURL());
        Parent root = loader.load();
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Scene scene = new Scene(root);
        window.setScene(scene);
        window.show();
    }

    /**
     * Initializes the read army buttons from the second page
     */
    public void initialize() {
        CSVFileHandler handle = new CSVFileHandler();
        area1.setText(handle.readHumanArmy());
        area2.setText(handle.readDevilArmy());
    }
}
