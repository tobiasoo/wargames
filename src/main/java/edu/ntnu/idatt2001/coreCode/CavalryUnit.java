package edu.ntnu.idatt2001.coreCode;

/**
 * The subclass CavalryUnit which is an extension of the super class Unit
 * It contains two abstract methods which determines your bonus attack damage and/or bonus resistance
 * It also contains two abstract methods which determines your bonus terrain damage and/or terrain bonus resistance
 * @author Tobias Orø
 */

public class CavalryUnit extends Unit {

    private int counter;
    /**
     * Constructor for the CavalryUnit class
     * The CavalryUnit class is an extension of the super class Code.Unit, and will thus inherit everything from this class
     * @param name name of the type
     * @param health health points of the unit
     */
    public CavalryUnit (String name, int health) {
        super(name, health, 20, 12);
    }
    /**
     * Abstract method for returning bonus attack damage
     * The Cavalry class has a special ability "Charge", where the first attack will do 6 bonus damage,
     * and all remaining attacks will do 2 bonus damage
     * @return charge damage
     */
    @Override
    public int getAttackBonus() {
        int attackBonus = 2;
        counter++;
        if (counter == 1) {
            return attackBonus + 4;
        } else {
            return attackBonus;
        }
    }
    /**
     * Abstract method for returning bonus resistance
     * @return 1 bonus resistance
     */
    @Override
    public int getResistBonus() {
        return 1;
    }

    /**
     * Abstract method for returning terrain bonus attack damage
     * Cavalries has a bonus in Plains
     * @param terrain the terrain that the battle will take place in
     * @return terrain attack damage bonus
     */
    @Override
    public int getTerrainAttackBonus(String terrain) {
        int terrainBonus = 0;
        if (terrain.equalsIgnoreCase("Plains")) {
            return terrainBonus + 4;
        }
        return terrainBonus;
    }

    /**
     * Abstract method for returning terrain bonus armor
     * @param terrain the terrain that the battle will take place in
     * @return terrain resist bonus
     */

    @Override
    public int getTerrainResistBonus(String terrain) {
        int terrainBonus = 0;
        if (terrain.equalsIgnoreCase("Forest")) {
            return getResistBonus() - 1;
        } return terrainBonus;
    }
}
