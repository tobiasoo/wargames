package edu.ntnu.idatt2001.coreCode;

/**
 * The subclass InfantryUnit which is an extension of the super class Unit
 * It contains two abstract methods which determines your bonus attack damage and/or bonus resistance
 * It also contains two abstract methods which determines your bonus terrain damage and/or terrain bonus resistance
 * @author Tobias Orø
 */

public class InfantryUnit extends Unit {
    /**
     * Constructor for the InfantryUnit class
     * The InfantryUnit class is an extension of the super class Unit, and will thus inherit everything form this class
     * @param name name of the type
     * @param health health points of the unit
     */
    public InfantryUnit (String name, int health) {
        super(name, health, 15, 10);
    }
    /**
     * Abstract method for getting bonus attack damage
     * @return 2 bonus damage
     */
    @Override
    public int getAttackBonus() {
        return 2;
    }
    /**
     * Abstract method for getting bonus resistance
     * @return 1 bonus resistance
     */
    @Override
    public int getResistBonus() {
        return 1;
    }

    /**
     * Abstract method for returning terrain bonus attack damage
     * @param terrain the terrain that the battle will take place in
     * @return terrain attack damage bonus
     */

    @Override
    public int getTerrainAttackBonus(String terrain) {
        int terrainBonus = 0;
        if (terrain.equalsIgnoreCase("Forest")) {
            return terrainBonus + 3;
        } return terrainBonus;
    }

    /**
     * Abstract method for returning terrain bonus armor
     * @param terrain the terrain that the battle will take place in
     * @return terrain resist bonus
     */
    @Override
    public int getTerrainResistBonus(String terrain) {
        int terrainBonus = 0;
        if (terrain.equalsIgnoreCase("Forest")) {
            return terrainBonus + 3;
        } return terrainBonus;
    }

}
