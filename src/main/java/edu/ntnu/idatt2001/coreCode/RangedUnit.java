package edu.ntnu.idatt2001.coreCode;

/**
 * The subclass RangedUnit which is an extension of the super class Unit
 * It contains two abstract methods which determines your bonus attack damage and/or bonus resistance
 * It also contains two abstract methods which determines your bonus terrain damage and/or terrain bonus resistance
 * @author Tobias Orø
 */

public class RangedUnit extends Unit {

    private int healthPoints2 = 100;

    /**
     * Attribute for the getResistBonus method
     */

    /**
     * Constructor for the RangedUnit class
     * The RangedUnit class is an extension of the super class .Unit, and will thus inherit everything from this class
     *
     * @param name   name of the type
     * @param health health points of the unit
     */

    public RangedUnit(String name, int health) {
        super(name, health, 15, 8);
    }

    /**
     * Abstract method for getting bonus attack damage
     *
     * @return 3 damage
     */
    @Override
    public int getAttackBonus() {
        return 3;
    }

    /**
     * Abstract method for getting bonus resistance
     * The resist bonus is incrementing by 2 until it reaches 2 resistance
     *
     * @return resist resistance
     */

    @Override
    public int getResistBonus() {
        int resistBonus = 2;
        int healthPoints = getHealth();

        if (healthPoints == 100) {
            return resistBonus + 4;
        }
        else if (healthPoints < healthPoints2) {
            healthPoints2 = 0;
            return resistBonus + 2;
        } else {
            return resistBonus;
        }
    }

    /**
     * Abstract method for returning terrain bonus attack damage
     * @param terrain the terrain that the battle will take place in
     * @return terrain attack damage bonus
     */

    @Override
    public int getTerrainAttackBonus(String terrain) {
        int terrainBonus = 0;
        if (terrain.equalsIgnoreCase("Hill")) {
            return terrainBonus + 3;
        }
        if (terrain.equalsIgnoreCase("Forest")) {
            return getAttackBonus() - 1;
        } return terrainBonus;
    }

    /**
     * Abstract method for returning terrain bonus armor
     * @param terrain the terrain that the battle will take place in
     * @return terrain resist bonus
     */

    @Override
    public int getTerrainResistBonus(String terrain) {
        return 0;
    }

}