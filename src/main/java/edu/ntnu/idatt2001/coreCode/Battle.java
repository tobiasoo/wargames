package edu.ntnu.idatt2001.coreCode;

/**
 * The Battle class.
 * It contains two methods, one which fills the list with units and one which simulates the battle
 * @author Tobias Orø
 */
public class Battle {
    /**
     * Attributes for the Battle class
     */
    private final Army armyOne;
    private final Army armyTwo;

    /**
     * Constructor for the Battle class, which stores two armies in the parameter
     *
     * @param armyOne The first army
     * @param armyTwo The second army
     */
    public Battle(Army armyOne, Army armyTwo) {
        this.armyOne = armyOne;
        this.armyTwo = armyTwo;
    }

    /**
     * The simulated battle between two armies
     * The counter variable makes this a turn based battle (1 % 2 == 1, 2 % 2 == 0 and so on)
     * @return armyOne or armyTwo depending on whom wins
     */

    public Army simulate(String terrain) {

        int counter = 0;
        while (armyOne.hasUnits() && armyTwo.hasUnits()) {
            counter++;
            if (counter % 2 == 0) {
                Unit randomUnit = armyTwo.getRandom();
                armyOne.getRandom().attack(randomUnit, terrain);

                if (randomUnit.getHealth() <= 0) {
                    armyTwo.remove(randomUnit);
                }
            } else {
                Unit randomUnit = armyOne.getRandom();
                armyTwo.getRandom().attack(randomUnit, terrain);

                if (randomUnit.getHealth() <= 0) {
                    armyOne.remove(randomUnit);
                }
            }
            }
        if (armyOne.hasUnits()) {
            return armyOne;
        } else {
            return armyTwo;
        }
    }

    /**
     * toString method for the Battle class
     * @return information regarding the battle
     */
    @Override
    public String toString() {
        return "Army 1: " + armyOne +
                "Army 2: " + armyTwo + "\n";
    }
}
