package edu.ntnu.idatt2001.coreCode;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * Army class
 * Contains methods to create an army
 * Contains methods to add or remove units from an army
 * @author Tobias Orø
 */
public class Army {
    /**
     * Attributes for the Army class
     */
    private final ArrayList<Unit> units;
    private final String name;

    /**
     * Constructor which creates a new list for the units
     * @param name the name of the unit
     * @param units the units in the list
     */
    public Army (String name, ArrayList units) {
        this.units = new ArrayList<>();
        this.name = name;
    }

    /**
     * Constructor with name as parameter
     * @param name the name of the unit
     */
    public Army (String name) {
        this.name = name;
        units = new ArrayList<>();
    }


    /**
     * Getter for name
     * @return units.getName return the name of the unit
     */
    public String getName () {
        for (Unit units : units) {
            return units.getName();
        } return "";
    }

    /**
     * Checks if the unit list is empty or not
     * @return true or false depending on if the list is empty or not
     */
    public boolean hasUnits () {
        return !units.isEmpty();
    }

    /**
     * Getter for infantry units
     * @return Infantry units
     */
    public ArrayList<Unit> getInfantryUnits() {
        ArrayList<Unit> infantryUnits;
        infantryUnits = units.stream().filter(units -> units.getName().equals("InfantryUnit")).collect(Collectors.toCollection(ArrayList::new));
        return infantryUnits;
    }

    /**
     * Getter for cavalry units
     * @return Cavalry units
     */

    public ArrayList<Unit> getCavalryUnits() {
        ArrayList<Unit> cavalryUnits;
        cavalryUnits = units.stream().filter(units -> units.getName().equals("CavalryUnit")).collect(Collectors.toCollection(ArrayList::new));
        return cavalryUnits;
    }

    /**
     * Getter for ranged units
     * @return Ranged units
     */
    public ArrayList<Unit> getRangedUnits() {
        ArrayList<Unit> rangedUnits;
        rangedUnits = units.stream().filter(units -> units.getName().equals("RangedUnit")).collect(Collectors.toCollection(ArrayList::new));
        return rangedUnits;
    }

    /**
     * Getter for commander units
     * @return Commander units
     */
    public ArrayList<Unit> getCommanderUnits() {
        ArrayList<Unit> commanderUnits;
        commanderUnits = units.stream().filter(units -> units.getName().equals("CommanderUnit")).collect(Collectors.toCollection(ArrayList::new));
        return commanderUnits;
    }

    /**
     * Adds a unit to the list
     * @param unit the unit
     */
    public void add (Unit unit) {
        units.add(unit);
    }

    /**
     * Adds all units to the list
     * @param newUnit all units
     */
    public void addAll (ArrayList <Unit> newUnit) {
        units.addAll(newUnit);
    }


    /**
     * Removes a unit from the list
     * @param unit the unit that gets removed
     */
    public void remove (Unit unit) {
            units.remove(unit);
        }

    /**
     * Method to get all units at once from the list
     * @return units in the list
     */
    public ArrayList<Unit> getAllUnits () {
        return units;
    }

    /**
     * Method that returns a random unit from the list
     * @return randomUnit a random unit from the list
     */
    public Unit getRandom () {
        Random random = new Random();
        int randomUnit = random.nextInt(units.size());
        return units.get(randomUnit);
    }

    /**
     * Shows all units as a number
     * @return units as an int
     */

    public int showUnits () {
       return units.size();
    }

    /**
     * Equals method for the Army class
     * @param o object of compare to
     * @return returns a boolean value of either true or false depending on if the objects are equal or not
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Army army = (Army) o;
        return Objects.equals(units, army.units) && Objects.equals(name, army.name);
    }

    /**
     * Hashcode method
     * @return returns hashcode of objects
     */
    @Override
    public int hashCode() {
        return Objects.hash(units, name);
    }

    /**
     * toString method for the class Army
     * @return Returns a String with information regarding the units name and how many units are left in the army
     */
    @Override
    public String toString() {
        return "\n" + getName() + getAllUnits() + "\n";
    }
}