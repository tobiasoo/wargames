package edu.ntnu.idatt2001.coreCode;

import java.util.ArrayList;

/**
 * The UnitFactory class
 * It has two methods;
 * One that creates a single unit, and one that creates n number of units and puts the units in a list
 * @author Tobias Orø
 */
public class UnitFactory {

    /**
     * Method that creates a single unit
     * @param name name of the unit
     * @param health health of the unit
     * @return null if the units where not found
     */
    public static Unit createUnit(String name, int health) {

        if (name.equalsIgnoreCase("Pikemen")|| name.equalsIgnoreCase("Imp") || name.equalsIgnoreCase("InfantryUnit")  && health == 100) {
            return new InfantryUnit(name, health);
        }
        if (name.equalsIgnoreCase("RangedUnit") || name.equalsIgnoreCase("Magogs") || name.equalsIgnoreCase("Marksman") && health == 100) {
            return new RangedUnit(name, health);
        }
        if (name.equalsIgnoreCase("CavalryUnit") || name.equalsIgnoreCase("Knight") || name.equalsIgnoreCase("Dread Knight") && health == 100) {
            return new CavalryUnit(name, health);
        }
        if (name.equalsIgnoreCase("CommanderUnit") || name.equalsIgnoreCase("Arch Angel") || name.equalsIgnoreCase("Arch Devil") && health == 180) {
            return new CommanderUnit(name, health);
        }
        return null;
    }

    /**
     * Creates n number of units and stores them in a list
     * @param n number of units
     * @param name name of the unit
     * @param health health of the unit
     * @return The unit list
     */
    public static ArrayList<Unit> getUnitsSortedByType (int n, String name, int health) {
        if (n < 1) {
            throw new IllegalArgumentException("No units selected.");
        }
        ArrayList<Unit> units = new ArrayList<>();
        while (units.size() < n) {

            if (name.equalsIgnoreCase("Pikemen") || name.equalsIgnoreCase("Imp") || name.equalsIgnoreCase("InfantryUnit") && health == 100) {
                units.add(new InfantryUnit(name, health));
            }
            if (name.equalsIgnoreCase("Marksman") || name.equalsIgnoreCase("Magogs") || name.equalsIgnoreCase("RangedUnit") && health == 100) {
                units.add(new RangedUnit(name, health));
            }
            if (name.equalsIgnoreCase("Knight") || name.equalsIgnoreCase("Dread Knight") || name.equalsIgnoreCase("CavalryUnit") && health == 100) {
                units.add(new CavalryUnit(name, health));
            }
            if (name.equalsIgnoreCase("CommanderUnit") || name.equalsIgnoreCase("Arch Angel") || name.equalsIgnoreCase("Arch Devil") && health == 180) {
                units.add(new CommanderUnit(name, health));

            }
        }
        return units;
    }
}