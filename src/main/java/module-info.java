module edu.ntnu.idatt2001.coreCode {

    requires javafx.graphics;
    requires javafx.controls;
    requires javafx.fxml;
    requires com.opencsv;


    opens edu.ntnu.idatt2001.coreCode to javafx.fxml;
    exports edu.ntnu.idatt2001.coreCode;
    exports edu.ntnu.idatt2001.coreCode.Controllers;
    opens edu.ntnu.idatt2001.coreCode.Controllers to javafx.fxml;

}