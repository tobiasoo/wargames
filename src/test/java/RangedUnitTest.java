import edu.ntnu.idatt2001.coreCode.InfantryUnit;
import edu.ntnu.idatt2001.coreCode.RangedUnit;
import edu.ntnu.idatt2001.coreCode.Unit;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RangedUnitTest {
    Unit unit1 = new RangedUnit("Archer", 100);
    Unit unit2 = new InfantryUnit("Pikemen", 100);
    @Test
    void getAttackBonus() {

        unit1.attack(unit2, "hill");
        assertEquals(3, unit1.getAttackBonus());
    }

    @Test
    void testGetResistBonus() {
        assertEquals(6, unit1.getResistBonus());
        unit2.attack(unit1, "forest");
        assertEquals(4, unit1.getResistBonus());
        unit2.attack(unit1, "Forest");
        assertEquals(2, unit1.getResistBonus());
        }

        @Test
        void getTerrainAttackDamage () {
            unit1.attack(unit2, "Plain");
            assertEquals(3, unit1.getTerrainAttackBonus("Hill"));
            unit1.attack(unit2, "Plain");
            assertEquals(2, unit1.getTerrainAttackBonus("Forest"));

        }
    }