import edu.ntnu.idatt2001.coreCode.InfantryUnit;
import edu.ntnu.idatt2001.coreCode.RangedUnit;
import edu.ntnu.idatt2001.coreCode.Unit;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SecondPageControllerTest {

    Unit unit1 = new InfantryUnit("Mads", 100);
    Unit unit2 = new RangedUnit("Randi", 100);

    @Test
    void chooseTerrains() {
        assertEquals(3, unit1.getTerrainAttackBonus("Forest"));
        assertEquals(2, unit2.getTerrainAttackBonus("Forest"));
    }
}