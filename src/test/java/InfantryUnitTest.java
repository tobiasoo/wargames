import edu.ntnu.idatt2001.coreCode.InfantryUnit;
import edu.ntnu.idatt2001.coreCode.RangedUnit;
import edu.ntnu.idatt2001.coreCode.Unit;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InfantryUnitTest {

    @Test
    void getTerrainAttackBonus() {
        Unit unit1 = new InfantryUnit("Pikeman", 100);
        Unit unit2 = new RangedUnit("Cyclops", 100);
        unit1.attack(unit2, "Forest");
        assertEquals(3, unit1.getTerrainAttackBonus("Forest"));

    }

    @Test
    void getAttackBonus() {
        Unit unit1 = new InfantryUnit("Pikeman", 100);
        Unit unit2 = new RangedUnit("Cyclops", 100);
        unit1.attack(unit2, "Forest");
        assertEquals(2, unit1.getAttackBonus());
    }

    @Test
    void getResistBonus () {
        Unit unit1 = new InfantryUnit("Pikeman", 100);
        Unit unit2 = new RangedUnit("Cyclops", 100);
        unit2.attack(unit1, "Forest");
        assertEquals(1, unit1.getResistBonus());
    }

    @Test
    void getTerrainResistBonus () {
        Unit unit1 = new InfantryUnit("Pikeman", 100);
        Unit unit2 = new RangedUnit("Cyclops", 100);
        unit2.attack(unit1, "Forest");
        assertEquals(3, unit1.getTerrainResistBonus("Forest"));
    }
}