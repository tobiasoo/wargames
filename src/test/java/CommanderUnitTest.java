import edu.ntnu.idatt2001.coreCode.CommanderUnit;
import edu.ntnu.idatt2001.coreCode.InfantryUnit;
import edu.ntnu.idatt2001.coreCode.Unit;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CommanderUnitTest {
    Unit unit1 = new CommanderUnit("Arch Angel", 100);
    Unit unit2 = new InfantryUnit("Pikemen", 100);
    @Test
    void getAttackBonus() {
        assertEquals(6, unit1.getAttackBonus());
        unit1.attack(unit2, "Forest");
        assertEquals(2, unit1.getAttackBonus());
        unit2.attack(unit1, "Hill");
        assertEquals(2, unit1.getAttackBonus());
    }

    @Test
    void getResistBonus() {
        unit2.attack(unit1, "Forest");
        assertEquals(1, unit1.getResistBonus());
    }

    @Test
    void getTerrainAttackBonus() {
        unit1.attack(unit2, "Plains");
        assertEquals(4, unit1.getTerrainAttackBonus("Plains"));
    }

    @Test
    void getTerrainResistBonus() {
        unit2.attack(unit1, "Forest");
        assertEquals(0, unit1.getTerrainResistBonus("Forest"));
    }
}