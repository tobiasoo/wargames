import edu.ntnu.idatt2001.coreCode.Army;
import edu.ntnu.idatt2001.coreCode.Battle;
import edu.ntnu.idatt2001.coreCode.Unit;
import edu.ntnu.idatt2001.coreCode.UnitFactory;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class BattleTest {
    Army army1 = new Army("Humans");
    Army army2 = new Army("Devils");
    Battle battle = new Battle(army1, army2);

    @Test
    void simulate() {
        ArrayList<Unit> unit1 = UnitFactory.getUnitsSortedByType(10, "Haaland", 100);
        ArrayList<Unit> unit2 = UnitFactory.getUnitsSortedByType(1, "Pogba", 100);

        army1.addAll(unit1);
        army2.addAll(unit2);
        battle.simulate("Forest");
        assertEquals(10, unit1.size());
    }
}